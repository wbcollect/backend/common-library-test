package com.wbcollect.initializers;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Use this container if you start your application with properties (i.e. SASL auth + NO SSL):
 * spring.kafka.properties.security.protocol=SASL_PLAINTEXT
 * spring.kafka.properties.sasl.mechanism=PLAIN
 * spring.kafka.properties.sasl.jaas.config=org.apache.kafka.common.security.plain.PlainLoginModule required username="testuser" password="testuser-secret";
 */
public class KafkaSASLInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    final static Map<String, String> env = new LinkedHashMap<>() {
        {
            put("KAFKA_LISTENER_SECURITY_PROTOCOL_MAP", "BROKER:PLAINTEXT,PLAINTEXT:SASL_PLAINTEXT");
            put("KAFKA_LISTENER_NAME_PLAINTEXT_SASL_ENABLED_MECHANISMS", "PLAIN");
            put("KAFKA_LISTENER_NAME_PLAINTEXT_PLAIN_SASL_JAAS_CONFIG", "org.apache.kafka.common.security.plain.PlainLoginModule required " +
//                    "username=\"admin\" " /*inter-broker user*/ + "password=\"admin-secret\" " +
                    "user_testuser=\"testuser-secret\" " +
                    ";");
        }
    };

    public static final KafkaContainer KAFKA_CONTAINER = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"))
            .withEmbeddedZookeeper()
            .withEnv(env);

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        KAFKA_CONTAINER.start();

        TestPropertyValues.of(
                "spring.kafka.bootstrap-servers=" + "SASL_" + KAFKA_CONTAINER.getBootstrapServers(),
                "spring.kafka.properties.security.protocol=SASL_PLAINTEXT"
        ).applyTo(applicationContext);
    }
}
