package com.wbcollect.initializers;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

public class PostgresInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static final PostgreSQLContainer POSTGRE_SQL_CONTAINER = (PostgreSQLContainer) new PostgreSQLContainer("postgres:14.3")
            .withDatabaseName("sampletest")
            .withUsername("postgres")
            .withPassword("postgres")
            .withCreateContainerCmdModifier(ContainerHelper.withMemoryLimit(128));

    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        System.out.println("POSTGRE_SQL_CONTAINER.getJdbcUrl()="+POSTGRE_SQL_CONTAINER.getJdbcUrl());
        TestPropertyValues.of(
                "spring.datasource.url=" + POSTGRE_SQL_CONTAINER.getJdbcUrl(),
                "spring.datasource.username=" + POSTGRE_SQL_CONTAINER.getUsername(),
                "spring.datasource.password=" + POSTGRE_SQL_CONTAINER.getPassword()
        ).applyTo(configurableApplicationContext.getEnvironment());
    }
}
