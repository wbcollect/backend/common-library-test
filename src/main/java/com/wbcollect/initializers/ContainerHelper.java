package com.wbcollect.initializers;

import com.github.dockerjava.api.command.CreateContainerCmd;

import java.util.function.Consumer;

class ContainerHelper {

    static Consumer<CreateContainerCmd> withMemoryLimit(int megabytes) {
        return cmd -> cmd.getHostConfig().withMemory(megabytes * 1024 * 1024L);
    }
}
