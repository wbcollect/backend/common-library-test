package com.wbcollect.initializers;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.utility.DockerImageName;

public class MockServerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static final MockServerContainer MOCK_SERVER_CONTAINER = new MockServerContainer(DockerImageName.parse("jamesdbloom/mockserver").withTag("mockserver-5.13.2"))
            .withCreateContainerCmdModifier(ContainerHelper.withMemoryLimit(64));

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        MOCK_SERVER_CONTAINER.start();
        System.out.println("MOCK_SERVER_CONTAINER.getHost() = " + MOCK_SERVER_CONTAINER.getHost());
        System.out.println("MOCK_SERVER_CONTAINER.getEndpoint() = " + MOCK_SERVER_CONTAINER.getEndpoint());
        System.out.println("MOCK_SERVER_CONTAINER.getServerPort() = " + MOCK_SERVER_CONTAINER.getServerPort());
        TestPropertyValues.of(
                "url.content=" + MOCK_SERVER_CONTAINER.getEndpoint() + "/content",
                "url.client=" + MOCK_SERVER_CONTAINER.getEndpoint() + "/client"
        ).applyTo(applicationContext);
    }
}
