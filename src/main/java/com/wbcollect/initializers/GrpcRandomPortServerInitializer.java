package com.wbcollect.initializers;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.net.ServerSocket;

public class GrpcRandomPortServerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        try (ServerSocket serverSocket = new ServerSocket(0)) {
            TestPropertyValues.of(
                    "grpc.server.port" + "=" + serverSocket.getLocalPort()
            ).applyTo(applicationContext);
        } catch (IOException e) {
            throw new RuntimeException();
        }

    }
}
