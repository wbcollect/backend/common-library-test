package com.wbcollect.initializers;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import static com.wbcollect.initializers.ContainerHelper.withMemoryLimit;

public class KafkaInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static final KafkaContainer KAFKA_CONTAINER = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"))
            .withCreateContainerCmdModifier(withMemoryLimit(1024)); // increased limit, otherwise container fails to start

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        KAFKA_CONTAINER.start();

        TestPropertyValues.of(
                "spring.kafka.bootstrap-servers=" + KAFKA_CONTAINER.getBootstrapServers()
        ).applyTo(applicationContext);
    }
}
